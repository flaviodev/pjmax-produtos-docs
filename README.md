## Documentos do Projeto

Espaço dedicado à publicações de tópicos e documentações pertinentes ao projeto, porém que não estão associados diretamente ao código. Os Assuntos estão divididos nos seguintes tópicos:

* [Ambiente de Desenvolvimento](ambiente-desenvolvimento/README.md)  
* [Decisões de Projeto](decisoes-projeto/README.md)  

