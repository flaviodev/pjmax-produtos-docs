## Ambiente de Desenvolbimento

* Sistema Operacional: Ubuntu 18.04
* IDE Java: Eclipse 2018-09
* IDE Angular: MS Visal Studio Code
* Criação do projeto backend: https://start.spring.io/
* Docker: 18.09
* Docker Compose: 1.17.1
* PostgreSQL: 9.6
