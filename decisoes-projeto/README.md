## Decisões de Projeto e Boas Práticas

O presente documento tem por objetivo relatar de fora suscinta as avaliações e decisões tomadas ao longo da execução do projeto proposto. 

### Criação de ambiente de desenvolvimento/execução baseados em docker/docker-compose

* A escolha da tecnologia permite tanto a geração rápida do ambiente de desenvolvimento, como a redução dos riscos de conflitos de versões da ferramentas (principalmente node.js/npm e angular)
* Facilita o build e execução do projeto em qualquer máquina que possua docker e docker-compose

### Criação do projeto: Criar um único projeto para implementação do backend e frontend ou criar projetos separados?

* Apesar da inclinação pela criação de dois projetos -- repositórios de código -- visando assim gerar dois artefatos independentes, possibilitando uma abordagem de microsserviços (deploy/scale independentes), para o objetivo do presente trabalho optou-se por desenvolver utilizano um único repositório 

### Utilização do Projeto lombok

* Cogitou-se inicialmente utilizar o projeto lombok visando um código menos verboso, porém como depende de uma configuração específica no IDE, logo a sua utilização foi descartada

### Nomes das tabelas, campos e demais elementos do banco de dados 

* Optou-se por gerar os nomes das tabelas e campos conforme o padrão de geração do jpa, ou seja, não nomear manualmente tais propriedades. Os únicos casos que se fará a nomeação manual será para os casos de nomeação de constraints como uniques e foreign keys, visando facilitar a identificação de violações de integridade do banco de dados

### Normalização do banco de dados proposto

Ao avaliar o MER do banco de dados proposto e aplicar as três primeiras formas normais, observou-se duas "não conformidades" com as formas normais, conforme abaixo:

* Preço do produto gravado na tabela do item do pedido: Uma vez que o preço está associado ao produto -- em sua respectiva tabela -- sua repetição de valor na tabela de item de produto viola a princípio a segunda forma normal, tendo por principal problema a possibilidade de ocorrência de anomalias de alteração. Contudo nesse caso optou-se por manter o modelo proposto, pois considerando a regra de negócio tal repetição faz sentido, uma vez que se deseja de fato fazer uma cópia do valor, de modo que, ao se alterar o valor do preço do produto os pedidos já realizados não sofram modificação.

* Totalização de valores nas tabelas pedido e item do pedido: Já este caso fere a terceira forma normal -- de se guardar valores resultantes de cálculos. Como não se identificou elementos suficientes que justifiquem a aplicação do modelo proposto, decidiu-se implementar os métodos de totalização nos objetos do modelo (transitent), sem no entanto armazená-los no banco de dados. 

### Mapeamento de Relacionamentos (JPA/Hibernate)

* Fetch LAZY: Inicialmente por não haver a necessidade explícita de se usar o EAGER -- para carregar prontamente algum registro. E associado a isso o fato de não se terem sido indentificados problemas com lentidão no carregamento -- devido ao escopo inicial e carga de dados inicial muito pequenos -- e lazy inicialization excetions, optou-se por mapear todos os relacionamentos com FetchType.LAZY

* Mapeamento somente das coleções derivadas de composição: Considerando questões de performance e evitar problemas como N+1, optou-se mapear somente a coleção dos itens do pedido na entidade pedido, por se tratar de uma composição. Sendo que para se obter os pedidos de um cliente ou os itens de pedido que estão associados a um produto, se darão apenas por meios de consultas (repositories) 

### Implementação de UUID ao invés de Serial

* Dentro do contexto dos micro serviços e cloud computing, existem vários cenários onde a simples geração de um valor sequencial pode não atender os requisitos de uma chave primária, entre eles: as aplicações estão distribuídas, podendo ser escaladas de forma mais granular, consistência de dados parcial (integridade parcial), utilização de tenancy... Considerado isto é uma boa prática adotar o UUID como valor para chaves primárias, pois há uma garantia maior de valor gerado não se repetir.

### Hibernate e Spring Data

* Considerando o requisito 2-e que trata da implementação utilizando Hibernate (JPA), considerou-se aqui a utilização do spring data cumpre o requisito (por depender e utilizar o hibernate-core), uma vez que o requisito parece mais remeter ao conceito ORM do que o uso da ferramenta em si. Considerou-se o uso mais explícito do hibernate -- principalmente com o uso de criterias entre outros -- porém em virtudo do prazo e das necessidades do projeto, optou-se pelo uso mais básico do spring data por meio de suas interfaces.

### Retorno da API como DTO e não utilizando a Entidade 

* Implementação de processo de conversão da Entidade em um objeto mais simples para o envio o transporte de dados para a apresentação, permitindo assim um melhor controle do que está indo para a apresentação, controle do tamanho do payload, dando mais coesão às classes (reponsabilidades melhores definidas)

### DDD - Regras de Negócio no domínio

* Implementação de regras de negócio do CRUD na entidade

### Commits pequenos

* Todas as implementações/alterações estão sendo quebradas em commits pequenos 

### Imagens Docker pequenas e Multi Stage Build

* Todas a imagens docker usadas para a execução do projeto sã baseadas em imagens alpine, reduzindo assim o consumo de recurso, o tempo de upload das imagens e os riscos associados a segurança
* No caso do container da aplicação java, por ser uma linguagem compilada optou-se por aplicar a técnica do multi stage build, com um Dockerfile com dois FROM, onde um realiza o build e outro gera a imagem com o artefato

### Arquitetura preparada para a implemetação dos testes unitários

* Apesar da ausência inicial da implementação dos testes unitários, a arquitetura foi concebida de modo que o código seja facilmente testável, uma vez que as regras de negócio estão nas entidades de domínio, basta mockear os acessos ao repositório.
